<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


    Route::get('/', function () {
        return view('welcome');
    });
Route::get('/room/join',function()
{
  return view('join');
});
//ROOMS
Route::post('/room/find','RoomController@find');
Route::post('/room/create','RoomController@create');
Route::get('/room/{secret}','RoomController@view');
Route::post('/room/{secret}/draw', 'RoomController@draw');
Route::post('/room/{secret}/getBoard','RoomController@getBoard');
//USER AUTH
Route::get('/register',function()
{
    return view('register');
})->middleware('guest');
Route::get('/home', function () {
    return view('welcome');
});
Route::get('/room/{secret}/admin','RoomController@getAdmin');
Route::get('/login',function()
{
    return view('login');
})->middleware('guest');
Route::post('/user/register','Auth\RegisterController@create');
Route::post('/user/login','Auth\LoginController@login');
Route::get('/user/logout','Auth\LoginController@logout');
Route::post('/user/auth', 'UserController@getUser');
Route::post('/room/{secret}/read-only','RoomController@makeRoomReadOnly');