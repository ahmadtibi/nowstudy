$(function () {

    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });

    $(".create-room").click(function () {
   $(".create-room_modal").modal('show');
});
    $("#get-started").click(function () {
       $('.create-room_modal').modal('show');
    });
$("#create-room_submit").click(function () {
   if($("#username").val().length === 0)
   {
       alert('Username cannot be empty');
       return false;
   }
   $.ajax({
      url:"/room/create",
      type:"POST",
       dataType:'json',
      success:function(data)
      {
          window.location.replace(`/room/${data.secret}?username=${$("#username").val()}`);
      },
      error:function () {
          alert('something went wrong');
      }
   });
});
});