$(function()
{
    "use strict";
    //We will use this to check if the user is drawing later on.
    let holdingDown = false;
    let usersDrawing  = [];
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var socket = io("http://"+window.location.host+":3000", {
        query : {
            token:sessionId
        }
    });
    var users = [];
    const room_id = window.location.pathname.split( '/' )[2];
    const  url_string = window.location.href;
    var url = new URL(url_string);
    const username = url.searchParams.get("username");
    socket.emit('join',room_id);
    socket.emit('register',username,room_id);
    // socket.emit('kick:user',room_id,'hey');
    canvas.on('shape:added', function (object) {
        $.ajax({
            url:'/room/'+room_id+'/draw',
            type:"POST",
            dataType:"json",
            data:{data:JSON.stringify(canvas.toJSON())},
            success:function(data)
            {
                window.test = canvas.toJSON();
                socket.emit('shape:added',{json:canvas.toJSON()});
            },
            //TODO: find a better fix for when the session expires.
            statusCode:{
              419: function()
              {
               window.location.reload();
              }
            },
            error:function(jqXHR, exception)
            {
                if(jqXHR.status === 401)
                {
                    alert('Cant draw as room is set in teacher mode (only the room creator can draw)');
                }
            }
        });
    });
    socket.on('user:join',function(user)
    {
        var json;
        $.ajax({
            url:'/room/'+room_id+'/getBoard',
            type:"POST",
            dataType:"json",
            success:function(data)
            {
                 callback(data.json);
            }
        });
        function callback(response) {
            json = response;
             socket.emit('draw:again', {json: json});
        }
        new Noty({
            type: 'success',
            layout: 'topCenter',
            theme:'bootstrap-v4',
            text: user.name + ' joined the room',
            timeout: 2000
        }).show();
        // $('.name-list').append(`<li>${user}</li>`);
    });
    //FIXME:i had to make this because the event user:join kept returning undefined and i had no solution one day i shall fix this XD!
    socket.on('get:users',function(usersInRoom,isAdmin)
    {
        // var user = usersInRoom[usersInRoom.users.length - 1];
        // var length = usersInRoom.users.length;
        // var user = usersInRoom.users[length-1];
        // users.push(user);
        // window.users = users;
        // usersInRoom.users.forEach(function(user) {
        //     users.push(user);
        //     $('.name-list').append(`<li>${user}</li>`);
        //     alert(users);
        // });
        let json = '';
        $.ajax({
            url:'/room/'+room_id+'/getBoard',
            type:"POST",
            dataType:"json",
            success:function(data)
            {
                callback(data.json);
            }
        });
        function callback(response) {
            json = response;
            socket.emit('draw:again', {json: json});
        }
        var appendedUsers = $(".list-users").map(function() {
            return $(this).text();
        }).get();
        for(let user of usersInRoom.users)
        {
            users.push(user);
            if(!appendedUsers.includes(user))
            {
                let date = new Date();
                $('.usersInRoom').append(`<tr>
                            <td class="list-users">${user}</td>
                            <td class="primary"><time class="timeago" datetime="${date.toISOString()}"></time></td>
                        </tr>`);
                $("time.timeago").timeago();
            }
            $.uniqueSort(users);
        }
    });
    $('.name-list').click(function()
    {
    });
    socket.on('draw:again',function(json)
    {
        window.json = json;
        canvas.loadFromJSON(json.json.json, canvas.renderAll.bind(canvas));
    });
    socket.on('draw:public', function(json)
    {
        canvas.loadFromJSON(json.object.object, canvas.renderAll.bind(canvas));
        canvas.renderAll();
    });
    socket.on('user:left',function(user)
    {
        new Noty({
            type: 'warning',
            layout: 'topCenter',
            theme:'bootstrap-v4',
            text: 'User left your channel: ' + user.user.username,
            timeout: 2000
        }).show();
        users.splice(users.indexOf(user),1);
        window.test = users;
        //Removes user from the table
        $(`.list-users:contains("${user.user.username}")`).parent().remove();
    });
    socket.on('shape:added',function(json)
    {
        window.json = json;
        canvas.loadFromJSON(json.object.json, canvas.renderAll.bind(canvas));
        //We add this to make sure that when the user draws a shape inside a shape it doesnt select the shape and mess up the whole shape
        if(tool != "cursor")
        {
            canvas.forEachObject(function (obj) {
                obj.selectable = false;
            })
        }
        console.error("GOT MESSAGE");
        // window.hey = object;
        // console.info(object.object.object);
        // canvas.add(object.object.object);
        // canvas.renderAll();
        // console.info(object);
    });
    socket.on('read:only',function(state)
    {
        if(state.state.state)
        {
            new Noty({
            type: 'warning',
            layout: 'topCenter',
            theme:'bootstrap-v4',
            text: 'Room owner has set the room to view only',
            timeout: 2000
        }).show();
        } else {
            new Noty({
            type: 'warning',
            layout: 'topCenter',
            theme:'bootstrap-v4',
            text: 'Room owner has changed the room from view only to view and edit',
            timeout: 2000
        }).show();
        }
    });
    $('#read-only').click(function()
    {
        var state;
        //User changed room from read only to view and edit.
        if($(this).hasClass('active'))
        {
            $(this).attr('data-tooltip','Prevents other users to draw anything')
            $('.desc').text('Teacher mode')
            $(this).removeClass('active');
            state = false;
        } else {
            $(this).attr('data-tooltip','Allows other users to draw anything')
            $('.desc').text('Free mode');
            $(this).addClass('active');
            state = true;
        }
        $.ajax({
            url:'/room/'+room_id+'/read-only',
            type:"POST",
            data:{},
            success:function(data)
            {
                    socket.emit('read:only',{state:state});
            }

        });
    });

    canvas.on('mouse:down',function()
    {
        holdingDown = true;
        //TODO: quick hack so that when a user draws over a rectangle or any shape or even a line it doesnt move that "thing" but rather draw over it. should find a better fix because this looks up every object on the canvas every time which could be not that performance efficient
        if(tool === "cursor") return;
        canvas.forEachObject(function(object){
            object.selectable = false;
        });
    });
    canvas.on('mouse:up',function () {
       holdingDown = false;
       socket.emit('stopped:drawing',{user:username});
        //TODO: quick hack so that when a user draws over a rectangle or any shape or even a line it doesnt move that "thing" but rather draw over it. should find a better fix because this looks up every object on the canvas every time which could be not that performance efficient
        if(tool === "cursor") return;
        canvas.forEachObject(function(object){
            object.selectable = false;
        });

    });
    canvas.on('mouse:move',function () {
        if(!holdingDown || tool == "cursor")
            return;
        socket.emit('drawing',{user:username});
    });
    socket.on('drawing',function (user) {
        $(".drawing").text(user.user + ' is drawing..');
    });
    socket.on('stopped:drawing',function (user) {
        $('.drawing').text('');
    });
    $(document).keypress(function (e) {
        if(e.keyCode == 13 && $.trim($('#send-message').val()).length > 0)
        {
            if($("#send-message").is(":focus"))
            {
                e.preventDefault();
                console.log('hey');
                sendMessage($("#send-message").val());
            }
        }
    })
    function sendMessage(message)
    {
        socket.emit('chat message',{message:message,user:username});
        $("#send-message").val('');
    }
    socket.on('chat message',function (message,user) {
        //XD i have no idea how i made thsi work but this shit is broken af but the emit returns message and user and apparently only the message has an array that contains both username and message Xd
        $(".messages-list").append(`<li class='direct-msg'><span class='author'>${message.user}:</span> ${message.message.message}</li>`)
        if($(".chat-box").hasClass("hidden"))
        {
            var value = parseInt($('.chat-counter').text());
            $('.chat-counter').text(value+1);
        }
    });
    $("#open-chat").click(function () {
        if($(".chat-box").hasClass("hidden"))
        {
            $(".chat-box").removeClass("hidden");
            $('.chat-counter').text(0);
            return;
        }
        $(".chat-box").addClass("hidden");
    })
    $(".close-chat").click(function () {
        $(".chat-box").addClass("hidden");
    })
    $(".kickUser").click(function () {
       let userToKick = $("#userToFind").val();
       socket.emit('kick:user',room_id,userToKick);
    });
       socket.on('redirectAfterKick', function (socketId) {
           alert('you were kicked from the room');
           window.location.href = "/";
       });
//     rect = new fabric.Rect({
//         left: 100,
//         top: 100,
//         fill: 'red',
//         width: 300,
//         height: 300
//     });
//
// // "add" rectangle onto canvas
//     canvas.add(rect);
});