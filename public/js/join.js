
$( document ).ready(function() {
    // Handler for .ready() called.
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#JoinRoom').click(function()
    {
        var RoomSecretKey = $('#RoomSecret').val();
        var username = $("#username").val();
        //If the user doesn't enter anything
        if(RoomSecretKey == "" || username == "")
        {
            $(".joinRoom").addClass('error');
        } else {
            $(".joinRoom").removeClass('error');
        }
        $.ajax({
            url:"/room/find",
            type:"POST",
            dataType:"json",
            data:{
                'secret':RoomSecretKey,
               'username':username
            },
            success:function(data)
            {
                window.location.replace('/room/'+RoomSecretKey+'?username='+username);
            },
            error:function(data)
            {
                console.log(data.status);
                //Room wasn't found.
                if(data.status == 404)
                {
                    $('.helperRoom').text("Room doesn't exist :(");
                }
                //User didn't enter a username.
                if(data.status == 500)
                {
                    $('.helperUsername').text("Enter a username");
                }

            }
        });
    });
});
