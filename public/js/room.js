$(function () {
    //variables
    let rect, line, circle, origX, origY, isMouseDown = false;
    let mouseX, mouseY;
    let color = 'black';
    //i.e thickness for the user
    let pencilstrokeWidth = 5;
    let strokeWidth = 5;
    //Random tips to display for the user.
    let tips = [['You can undo anything using the Control+Z shortcut','Select anything on the board to move/scale/rotate it.'],
                ['You can take a picture of the board by clicking the Snapshot button at the bottom of the page.'],['You can clear the board using the shift+delete shortcut.'],
        ['You can open up a calculator by clicking the calculator icon on the far left from the toolset','You can upload an image to the board by clicking on the image icon from the toolset (left of the screen)'],
        ['You can draw an equation by clicking on the icon of a pencil drawing from the toolset (left of the screen)'],['You can delete an object by selecting it and pressing delete'],
        ['You can open a menu by selecting an object on the board and then right clicking on it with the cursor tool']
    ];
    //Select a random tip as tip of the day
    let tipOfTheday = tips[Math.floor(Math.random()*tips.length)];
    new Noty({
        type: 'info',
        layout: 'topCenter',
        theme:'bootstrap-v4',
        text: `Did you know: ${tipOfTheday[0]}`,
        timeout: 2000
    }).show();
    //canvas constructor
     window.canvas = new fabric.Canvas("c", {
        isDrawingMode: false,
        width:2316,
         height:1149,
        backgroundColor: "white",
         preserveObjectStacking:false
    });
    $("#undo").click(function () {
        undo();
    });
    function undo() {
        if (canvas._objects.length > 0) {
            let object = canvas.item(canvas.getObjects().length - 1);
            canvas.remove(object);
            canvas.renderAll();
        }
    }
    function copyToClipboard(element) {
        let $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }
    $("#copy").trigger('click');
    $("#copy").on("click",function () {
        copyToClipboard('#roomSecret');
        new Noty({
            type: 'success',
            layout: 'topCenter',
            theme:'bootstrap-v4',
            text: "Room's secret key was successfully copied to clipboard",
            timeout: 2000
        }).show();
    });
    //max width of the canvas..
    let width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    //max height of the canvas..
    let height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    canvas.setHeight(height);
    canvas.setWidth(width);
    // canvas.freeDrawingBrush.color = 'transparent';
     tool = 'pencil';

    function changeStatus(value) {
        canvas.forEachObject(function (obj) {
            obj.selectable = value;
        })
        canvas.renderAll();
    }

    //For UX experience: when the user clicks or chooses a tool (thru the shortcuts) set that tool background color to #ddd.
    function selectEffect(classname) {
        if ($('.tool-selected').length > 0) {
            $('.tools-select').removeClass('tool-selected');
            $(classname).addClass('tool-selected');
        } else {
            $(classname).addClass('tool-selected');
        }
    }

    function selectEffectPopover(classname) {
        if ($('.popover-item-selected').length > 0) {
            $('.popover-item').removeClass('popover-item-selected');
            $(classname).addClass('popover-item-selected');
        } else {
            $(classname).addClass('popover-item-selected');
        }
    }

    function pencilTool() {
        tool = 'pencil';
        canvas.freeDrawingBrush.color = color;
        canvas.freeDrawingBrush.width = strokeWidth;
        canvas.isDrawingMode = true;
        selectEffect('.pencil-select');

    }

    function squareTool() {
        tool = 'square';
        canvas.selection = false;
        changeStatus(false);
        canvas.isDrawingMode = false;
        canvas.on('mouse:down', onMouseDown);
        canvas.on('mouse:move', onMouseMove);
        canvas.on('mouse:up', onMouseUp);
        selectEffect('.square-select');
    }

    function lineTool() {
        tool = 'line';
        canvas.selection = false;
        canvas.isDrawingMode = false;
        changeStatus(false);
        canvas.on('mouse:down', onMouseDown);
        canvas.on('mouse:move', onMouseMove);
        canvas.on('mouse:up', onMouseUp);
        selectEffect('.line-select');
    }

    function circleTool() {
        tool = 'circle';
        canvas.selection = false;
        canvas.isDrawingMode = false;

        changeStatus(false);
        canvas.on('mouse:down', onMouseDown);
        canvas.on('mouse:move', onMouseMove);
        canvas.on('mouse:up', onMouseUp);
        selectEffect('.circle-select');
    }

    function cursorTool() {
        tool = 'cursor';
        canvas.selection = true;
        canvas.isDrawingMode = false;
        changeStatus(true);
        canvas.forEachObject(function (obj) {
            obj.selectable = true;
        });
        //remove mouse event
        canvas.off('mouse:down', onMouseDown);
        canvas.off('mouse:move', onMouseMove);
        canvas.off('mouse:up', onMouseUp);
        selectEffect('.cursor-select');
    }
    function copyObject() {
        /* clone what are you copying since you
        // may want copy and paste on different moment.
        // and you do not want the changes happened
        // later to reflect on the copy. */

        //If user didn't select anything return null and exit out of the function
        if (canvas.getActiveObject() == null) {
            return;
        }
        canvas.getActiveObject().clone(function (cloned) {
            _clipboard = cloned;
            console.warn(cloned);
        });
    }

    function pasteObject() {
        // clone again, so you can do multiple copies.
        _clipboard.clone(function (clonedObj) {
            canvas.discardActiveObject();
            clonedObj.set({
                left: clonedObj.left + 10,
                top: clonedObj.top + 10,
                evented: true,
            });
            if (clonedObj.type === 'activeSelection') {
                // active selection needs a reference to the canvas.
                clonedObj.canvas = canvas;
                clonedObj.forEachObject(function (obj) {
                    canvas.add(obj);
                });
                // this should solve the unselectability
                clonedObj.setCoords();
            } else {
                canvas.add(clonedObj);
            }
            _clipboard.top += 50;
            _clipboard.left += 50;
            canvas.setActiveObject(clonedObj);
            canvas.renderAll();
        });
    }

    function clearBoard() {
        if (confirm("Are you sure you want to clear the board?")) {
            canvas.clear();
            canvas.backgroundColor = "#fff";
            canvas.renderAll();
            let canvasStr = JSON.stringify(canvas);
            canvas.fire('shape:added',  {json:canvasStr});
        } else {
            return;
        }
    }
    //TODO make file upload more secure
    function uploadFile(file,reader)
    {
        $.ajax({
            url:"/upload",
            method:"POST",
            type:"POST",
            data:file,
            success:function()
            {

            },
            error:function()
            {

            }
        });
    }
    //Uploads image
    document.getElementById('file').addEventListener("change", function (e) {
        let file = e.target.files[0];
        let reader = new FileReader();
        // uploadFile();
        reader.onload = function (f) {
            let img_width, img_height;
            let data = f.target.result;
            let img = new Image();
            img.onerror = function(error)
            {
              return false;
            };
            img.src = f.target.result;
            img.onload = function () {
                img_width = this.width;
                img_height = this.height;
            };
            fabric.Image.fromURL(data, function (img) {
                let  oImg = img.set({left: 0, top: 0, angle: 00, width: img_width, height: img_height}).scale(0.9);
                canvas.add(oImg);
                canvas.centerObject(oImg);
                let  a = canvas.setActiveObject(oImg);
                canvas.renderAll();
                let  dataURL = canvas.toDataURL({format: 'png', quality: 0.8});
                let canvasStr = JSON.stringify(canvas);
                // let canvasStr = canvas.toObject();
                window.canvasStr = canvasStr;
                canvas.fire('shape:added', {json: canvasStr});
            });
        };
        reader.readAsDataURL(file);
    });
    // canvas.freeDrawingBrush.width = 1;
    // canvas.freeDrawingBrush.color = '#ff0000';
    $('#pencil').click(function () {
        pencilTool();
    });
    $('#square').click(function () {
        squareTool();
    });
    $('#circle').click(function () {
        circleTool();
    });
    // $("#eraser").click(function () {
    //     eraserTool();
    // });
    $("#line").click(function () {
        lineTool();
    });
    $("#cursor").click(function () {
        cursorTool();
    });
    $(".calculator").on('click', function () {
        if ($('.sci-calc').is(':visible')) {
            //put your code if div is visible.
            $(".sci-calc").hide();
        } else {
            //put your code if div is not visible.
            $(".sci-calc").show();
        }
    });
    $("#clear").click(function () {
        clearBoard();
    });
    $("#black").click(function () {
        color = 'black';
        canvas.freeDrawingBrush.color = color;
        selectEffectPopover('.black');
    });
    $("#red").click(function () {
        color = 'red';
        canvas.freeDrawingBrush.color = color;
        selectEffectPopover('.red');
    });
    $("#green").click(function () {
        color = 'green';
        canvas.freeDrawingBrush.color = color;
        selectEffectPopover('.green');
    });
    $("#grey").click(function () {
        color = 'grey';
        canvas.freeDrawingBrush.color = color;
        selectEffectPopover('.grey');
    });
    $("#teal").click(function () {
        color = 'teal';
        canvas.freeDrawingBrush.color = color;
        selectEffectPopover('.teal');
    });
    $("#brown").click(function () {
        color = 'brown';
        canvas.freeDrawingBrush.color = color;
        selectEffectPopover('.brown');
    });
    $("#screenshot").click(function () {
        canvas.getElement().toBlob(function (blob) {
            saveAs(blob, "canvas.png");
        });
    });
    $("#equation").click(function()
    {
        $('#equation-modal').modal('show');
    });
    $("#equation-drawer").click(function()
    {
       $("#function-drawer-modal").modal('show');
    });
    $("#function-draw").click(function()
    {
       //Get the function the user submitted
       let func = $("#FunctionInput").val();
       drawFunction(func);
    });
    $("#manage").click(function()
    {
        $("#manage-users").modal('show');
    });
    function drawFunction(func) {
        try {
            functionPlot({
                target: '#plot',
                data: [{
                    fn: func,
                    sampler: 'builtIn',  // this will make function-plot use the evaluator of math.js
                    graphType: 'polyline'
                }]
            });
            //Fetch the svg that we just created from math js plot..
            let svgEl = document.body.getElementsByClassName('function-plot')[0];
            let serializer = new XMLSerializer();
            //Serialize it so we can call fabric js loadSVGFromString
            let svgStr = serializer.serializeToString(svgEl);
            //For debugging

            fabric.loadSVGFromString(svgStr, function(objects, options) {
                // Group elements to fabric.PathGroup (more than 1 elements) or
                // to fabric.Path
                let loadedObject = fabric.util.groupSVGElements(objects, options);
                // Set sourcePath
                loadedObject.set('sourcePath', svgStr);
                canvas.add(loadedObject);
                canvas.centerObject(loadedObject);
                canvas.renderAll();
                let canvasStr = JSON.stringify(canvas);
                canvas.fire('shape:added', {json: canvasStr});
            });
        }
        catch (err) {
            console.log(err);
            alert('Something went wrong :/' + err);
        }
    }
    let TeX;
    function UpdateMath(TeX)
    {
        TeX = "$" + TeX + "$";
        document.getElementById("MathOutput").innerHTML = TeX;
        //reprocess the MathOutput Element
        MathJax.Hub.Queue(["Typeset",MathJax.Hub,"MathOutput"]);
    }
    $("#MathInput").keyup(function()
    {
       TeX = this.value;
       UpdateMath(TeX);
    });
    function writeEquation()
    {
        html2canvas([document.getElementById('MathOutput')], {
            //We named this canvas_html2canvas because we already have fabric js canvas (let canvas)
            onrendered: function (canvas_html2canvas) {
                let imagedata = canvas_html2canvas.toDataURL('image/png');
                let equation = new fabric.Image.fromURL(imagedata, function(img)
                {
                    canvas.add(img);
                    canvas.centerObject(img);
                    let a = canvas.setActiveObject(img);
                    canvas.renderAll();
                    let canvasStr = JSON.stringify(canvas);
                    // let canvasStr = canvas.toObject();
                    window.canvasStr = canvasStr;
                    canvas.fire('shape:added', {json: canvasStr});
                });
            }
        });
    }
    $("#equation-write").click(function()
    {
        writeEquation();
    });
    function downloadCanvas(link, filename) {
        link.href = canvas.toDataURL('png');
        link.download = filename;
    }
    function deleteObject()
    {
        let activeObject = canvas.getActiveObjects();
        if (activeObject) {
            activeObject.forEach(function (object) {
                canvas.remove(object);
            });
            canvas.discardActiveObject();
        }
    }
    //Keyboard shortcuts
    window.onkeydown = function (e) {
        //If user pressed delete and the tool is cursor
        if (e.keyCode == 46 && tool == 'cursor') {
            deleteObject();
        }
        //If user pressed shift + S set the tool to straight line
        if (e.shiftKey && e.keyCode == 83) {
            lineTool();
        }
        //Control + Z shortcut for undo
        if (e.ctrlKey && e.keyCode == 90) {
            undo();
        }
        //Control + C shortcut for copying the selected object
        if (e.ctrlKey && e.keyCode == 67) {
            copyObject();
        }
        //Control + V shortcut for pasting the copied object from copyObject()
        if (e.ctrlKey && e.keyCode == 86) {
            pasteObject();
        }
        //If the user pressed shift + E set the tool to pencil
        if (e.shiftKey && e.keyCode == 69) {
            pencilTool();
        }
        //If the user pressed shift + delete
        if (e.shiftKey && e.keyCode == 46) {
            clearBoard();
        }
        //If the user pressed shift + R set the tool to square
        if (e.shiftKey && e.keyCode == 82) {
            squareTool();
        }
        //If the user pressed shift + C set the tool to circle
        if (e.shiftKey && e.keyCode == 67) {
            circleTool();
        }
        //If the user pressed A set the tool to cursor
        if (e.keyCode == 65) {
            cursorTool();
        }
    }

    function onMouseDown(o) {
        console.warn('clicked');
        isMouseDown = true;
        let pointer = canvas.getPointer(o.e);
        origX = pointer.x;
        origY = pointer.y;
        if (tool == 'square') {
            rect = new fabric.Rect({
                left: origX,
                top: origY,
                originX: 'left',
                originY: 'top',
                width: pointer.x - origX,
                height: pointer.y - origY,
                stroke: color,
                strokeWidth: strokeWidth,
                selectable: true,
                hoverCursor: 'cursor',
                fill: ''
            });
            canvas.add(rect);
        }
        if (tool == 'circle') {
            circle = new fabric.Circle({
                left: origX,
                top: origY,
                originX: 'left',
                originY: 'top',
                radius: pointer.x - origX,
                angle: 0,
                fill: '',
                stroke: color,
                selectable: true,
                hoverCursor: 'cursor',
                strokeWidth: strokeWidth
            });
            canvas.add(circle);
        }
        if (tool == 'line') {
            let points = [pointer.x, pointer.y, pointer.x, pointer.y];
            line = new fabric.Line(points, {
                strokeWidth: strokeWidth,
                fill: color,
                stroke: color,
                originX: 'center',
                originY: 'center',
                selectable: true,
                hoverCursor: 'cursor'
            });
            canvas.add(line);
        }
    }

    function onMouseMove(o) {
        if (!isMouseDown) {
            return;
        }

        let pointer = canvas.getPointer(o.e);
        if (tool == 'square') {
            if (origX > pointer.x) {
                rect.set({left: Math.abs(pointer.x)});
            }
            if (origY > pointer.y) {
                rect.set({top: Math.abs(pointer.y)});
            }
            rect.set({width: Math.abs(origX - pointer.x)});
            rect.set({height: Math.abs(origY - pointer.y)});
            canvas.renderAll();
        }
        if (tool == 'circle') {
            let radius = Math.max(Math.abs(origY - pointer.y), Math.abs(origX - pointer.x)) / 2;
            if (radius > circle.strokeWidth) {
                radius -= circle.strokeWidth / 2;
            }
            circle.set({radius: radius});

            if (origX > pointer.x) {
                circle.set({originX: 'right'});
            } else {
                circle.set({originX: 'left'});
            }
            if (origY > pointer.y) {
                circle.set({originY: 'bottom'});
            } else {
                circle.set({originY: 'top'});
            }
            canvas.renderAll();
        }
        if (tool == 'line') {
            line.set({x2: pointer.x, y2: pointer.y});
            canvas.renderAll();
        }
    }

    function  onMouseUp(o) {
        if (tool == 'square') {
            rect.setCoords();
            // canvas.fire('shape:added', { object:rect});

        }
        if (tool == 'circle') {
            circle.setCoords();
        }
        if (tool == 'line') {
            line.setCoords();
        }
        isMouseDown = false;
    }
    //Left and top so we can display the options right where the user hovered
    function showHoverOptions(left,top)
    {
        $('#hover-options').css("left",left);
        $('#hover-options').css('top',top);
        $('#hover-options').css('display','block !important');
        $('#hover-options').addClass('show');
    }
    // create a rectangle object
     rect = new fabric.Rect({
        left: 100,
        top: 100,
        fill: 'red',
        width: 300,
        height: 300
    });

// "add" rectangle onto canvas
    canvas.add(rect);
    canvas.on('mouse:move', function(options) {
        let hoveredOnObject;
        if(!options.target)
        {
            hoveredOnObject = false;
            return;
        }else{
            hoveredOnObject = true;
        }
       mouseX  = options.e.layerX;
       mouseY = options.e.layerY;
        if(hoveredOnObject) {
            if (tool != "cursor") {
                return;
            }
            window.oncontextmenu = function (e) {
                //Prevent the default right click
                if(tool == "cursor") {
                    e.preventDefault();
                    showHoverOptions(mouseX, mouseY);
                }
            };
        } else {
            return;
        }
       // showHoverOptions(mouseX,mouseY);
    });
    canvas.on('object:removed', function (object) {
        console.warn(object);
    });
    $('#hover-options').mouseleave(function () {
        $('#hover-options').removeClass('show');
    });
    canvas.on('mouse:over',function(object)
    {
        //If the user hovered on something that isn't an object
        canvas.hoverCursor = 'cursor';
        if(!object.target)
        {
            return;
        }
        //On right click
        // window.oncontextmenu = function(e) {
        //     console.log(object.target);
        //     if(tool != "cursor")
        //     {
        //         return false;
        //     }
        //     //Prevent the default right click
        //     e.preventDefault();
        //     showHoverOptions(mouseX, mouseY);
        // };
        $('#deleteObject').click(function()
        {
            deleteObject();
           $('#hover-options').removeClass('show');
        });
    });

    $("#pasteObject").click(function()
    {
        pasteObject();
        $('#hover-options').removeClass('show');
    });
    $("#copyObject").click(function()
    {
        copyObject();
        $('#hover-options').removeClass('show');
    });
    canvas.on('object:added', function (object) {
        // let canvasStr = canvas.toObject();
        // window.canvasStr = canvasStr;
        // canvas.fire('shape:added', {json: canvasStr});
    });
    canvas.on('mouse:up' , function(object)
    {
        if(tool == "cursor")
            return;
        let canvasStr = JSON.stringify(canvas);
        // let canvasStr = canvas.toObject();
        window.canvasStr = canvasStr;
        canvas.fire('shape:added', {json: canvasStr});
    });
    canvas.on('object:modified',function(object)
    {
        let canvasStr = JSON.stringify(canvas);
        canvas.fire('shape:added', {json:canvasStr});
    });
    canvas.on('path:created', function(e){
        let canvasStr = JSON.stringify(canvas);
        canvas.fire('shape:added', {json:canvasStr});
    });
    canvas.on('object:removed' , function(object)
    {
        let canvasStr = JSON.stringify(canvas);
        canvas.fire('shape:added' , {json:canvasStr});
    });
    $('#color-picker').click(function () {
        $(".popover-content").show();
    });
    $('.popover-content').mouseleave(function () {
        $('.popover-content').hide();
    });
    //todo make it work with other tools (something breaks when i change strokeWidth for other tools)
    $("#thickness").change(function()
    {
       let value = this.value;
       $(".thickness-value").text(value);
       pencilstrokeWidth = value;
       canvas.freeDrawingBrush.width = parseInt(pencilstrokeWidth);
       //debugging
    });
    /*  calculator code
    from here on
     */
    window.onkeypress = function (e) {
        //If the user doesnt have the calculator open then don't bother registering key strokes..
        if (!$('.sci-calc').is(':visible'))
        {
            return;
        }
        //Allowed keys for input
        let allowedKeys = ["Enter", "Backspace", "+", "-", "*", "/", "."];
        e.preventDefault();
        let pressedButton = e.key;
        //If the user typed something other than a number i.e Alt Control (excluding allowedKeys)
        if (isNaN(pressedButton) && $.inArray(pressedButton, allowedKeys) === -1) {
            return false;
        }
        if (pressedButton == "Enter") {
            $("#display").val(compute($("#display").val()));
            return;
        }
        if (pressedButton == "Backspace") {
            deleteChar();
            return;
        }
        // if(pressedButton == "Enter")
        // {
        //     compute($("#display").val());
        // }
        let display = $('#display').val();
        if (display != "0") {
            let val1 = display;
            let res = val1 + pressedButton;
            $("#display").val(res);
        } else {
            $("#display").val(pressedButton);
        }
    }
    $('.btnNum').click(function () {
        let pressedButton = $(this).val();
        //Get the numbers in the calculator display box
        let display = $('#display').val();
        if (display != "0") {
            let val1 = display;
            let res = val1 + pressedButton;
            $("#display").val(res);
        } else {
            $("#display").val(pressedButton);
        }
        //debugging
    });
    $(".btnMath").click(function () {
        let pressedButton = $(this).val();
        let display = $("#display").val();
        let val1 = display;
        let res = val1 + pressedButton;
        $("#display").val(res);
    });
    $("#compute").click(function () {
        let displayValue = $("#display").val();
        $("#display").val(compute(displayValue));
    });
    $("#deleteChar").click(function () {
        deleteChar();
    });
    $("#clearDisplay").click(function () {
        $("#display").val('');
    });
    $(".trigonometric_function").click(function () {
        let func = $(this).val();
        let number = $("#display").val();
        let value = trigonometric_functions(number, func);
        $("#display").val(value);
    });
    $(".absolute_value").click(function () {
        let value = $("#display").val();
        if (value.substring(0, 1) == "-") {
            value = value.substring(1, value.length);
            $("#display").val(value);
        } else {
            value = "-" + value;
            $("#display").val(value);
        }
    });
    $(".sqrt").click(function () {
        let value = $('#display').val();
        $("#display").val(sqrt(value));
    });
    $(".exponentiation").click(function () {
        let value = $("#display").val();
        $("#display").val(exponentiation_by_2(value));
    });
    $(".log").click(function () {
        let value = $("#display").val();
        $("#display").val(log(value));
    });
    $(".round").click(function () {
        let value = $("#display").val();
        $("#display").val(approx(value));
    });

    function compute(equation) {
        let result = eval(equation);
        return result;
    }

    function trigonometric_functions(number, func) {
        if (func == "sin") {
            return Math.sin(number * Math.PI / 180.0);
        }
        if (func == "cos") {
            return Math.cos(number * Math.PI / 180.0);
        }
        if (func == "tan") {
            return Math.tan(number * Math.PI / 180.0);
        }
        return false;
    }

    function sqrt(number) {
        return Math.sqrt(number);
    }

    function exponentiation_by_2(number) {
        return Math.pow(number, 2);
    }

    function log(number) {
        return Math.log(number);
    }

    function approx(number) {
        return Math.round(number);
    }

    function deleteChar() {
        let displayValue = $("#display").val();
        //Remove last number
        displayValue = displayValue.slice(0, -1);
        $("#display").val(displayValue);
    }

    // video code
    let canvas_2 = document.querySelector('#c');
    let recorder, is_recording, noAudio = false;

    function random_filename(len = "6") {
        let text = " ";

        let charset = "abcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < len; i++)
            text += charset.charAt(Math.floor(Math.random() * charset.length));

        return text;
    }

    function recordWithMic() {
        navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
        navigator.getUserMedia({
            audio: true
        }, function (microphone) {
            let canvasStream = canvas_2.captureStream(25);
            microphone.getAudioTracks().forEach(function (track) {
                // merge microphone into canvas stream
                canvasStream.addTrack(track);
            });

            // now your canvas stream has both audio and video tracks
            // now you can record it using RecordRTC
            recorder = RecordRTC(canvasStream, {
                type: 'video'
            });

            // // auto stop after 5 seconds recording
            // recorder.setRecordingDuration(5 * 1000).onRecordingStopped(function() {
            //     let url = recorder.toURL();
            //     window.open(url);
            //
            //     let blob = recorder.getBlob();
            //     let singleWebM = new File([blob], 'single.webm', {
            //         type: 'video/webm'
            //     });
            //     saveAs(blob,"test.webm");
            // });
            recorder.startRecording();
        }, function (error) {
            console.log(error);
            noAudio = true;
            record();
            // if (error.name == "NotAllowedError") {
            //     noAudio = true;
            //     record();
            // }
            // if (error.name == "AbortError") {
            //     alert("Something went wrong :(");
            // }
        });
    }

    //Code gets kinda messy here but i made sure to comment everything
    $("#record").click(function () {
        //If true then it means user isnt recording as stop recording is disabled ---> READ THIS
        is_recording = $('#stop-recording').hasClass('disabled');
        if (is_recording) {
            //Since he isn't recording enable the stop recording button
            $('#stop-recording').removeClass('disabled');
            // recorder = RecordRTC(canvas_2, {
            //     type: 'canvas'
            // });
            //Add disabled to the start recording button because now the user is recording there is no need for him to click record again
            $(this).addClass('disabled');
            // recorder.startRecording();
            //Record with mic
            recordWithMic();
        } else {
            return;
        }
    });
    $("#stop-recording").click(function () {
        //If stop recording is already disabled then the user shouldnt be clicking it anyways..
        if ($(this).hasClass('disabled')) {
            return false;
        } else {
            //Otherwise it means the user is recording and he clicked the stop recording button which means we should disable it again and enable the record button
            $(this).addClass('disabled');
            //If there is no audio then just record the canvas without audio
            if (noAudio) {
                recorder.stopRecording(function () {
                    recorder.clearRecordedData();
                    let blob = recorder.getBlob();
                    // document.body.innerHTML = '<video controls src="' + URL.createObjectURL(blob) + '" autoplay loop></video>';
                    saveAs(blob, random_filename(5) + '.webm');
                });
            }
            //Otherwise record with audio
            if (!noAudio) {
                console.log(noAudio);
                console.log("Stopped recording");
                recorder.stopRecording(function () {
                    recorder.clearRecordedData();
                    // let url = recorder.toURL();
                    // window.open(url);
                    let blob = recorder.getBlob();
                    let singleWebM = new File([blob], 'single.webm', {
                        type: 'video/webm'
                    });
                    saveAs(blob, random_filename(5) + '.webm');
                });
            }
            $("#record").removeClass("disabled");
        }
    });

    function record() {
        recorder = RecordRTC(canvas_2, {
            type: 'canvas'
        });
        recorder.startRecording();
        // setTimeout(function(){
        //     recorder.stopRecording(function() {
        //         let blob = recorder.getBlob();
        //         document.body.innerHTML = '<video controls src="' + URL.createObjectURL(blob) + '" autoplay loop></video>';
        //         saveAs()
        //     });
        // }, 5000);
    }
});