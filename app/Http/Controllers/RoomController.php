<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class RoomController extends Controller
{
    public function find(Request $request)
    {
        $secret = $request->secret;
        $username = $request->username;
        $room = Room::findOrFail($secret);
        if($room)
        {
            if(empty($username))
            {
                throw new \ErrorException('Error');
            }
            return response()->json($room);
        }
    }
	public function create(Request $request)
    {
        $secret = bin2hex(random_bytes(16));
        $admin = auth()->user()->username ?? null;
        $room = Room::create(['secret' => $secret, 'admin' => $admin, 'readonly' => '0']);
        if(!$room) return response()->json(['error' => 'Something went wrong :/'],500);
        return response()->json(['secret' => (string)$secret],200);
    }
    public function view($secret)
    {
        $room = Room::findOrFail($secret);
        $username = request()->query('username');
        if (empty($username) && !auth()->user()) {
//            return response()->view('errors.404', ['message' => 'Invalid cat id'], 404);
            abort(404, 'You must have a username in order to join the room. In order to set a username join the room through the join page on the website');
        }
        if ($room) {
            return view('room')->with('room',$room);
        }
        
    }
    public function makeRoomReadOnly($secret)
    {
        $room = Room::findOrFail($secret);
        $username = auth()->user()->username ?? null;
        if($room->admin != $username || !$username)
        { 
            return response()->json(['error' => 'Unauthorized'],401);
        } 
            if($room->readonly == 0)
            {
                $room->readonly = 1;
                $room->save();
                return;
            }
            $room->readonly = 0;
            $room->save();
    }

    public function getAdmin($secret)
    {
        $room = Room::find($secret);
//        if(!$room)
//        {
//            return response()->json(['error' => 'Couldnt fetch the admin from the provided room :/.'],404);
//        }
        return response()->json(['admin' =>$room->admin],200);
    }
    public function draw($secret,Request $request)
    {
        $room = Room::findOrFail($secret);
        if($room->readonly)
        {
            if(!Auth::user() || auth()->user()->username != $room->admin)
            {  
                return response()->json(['error' => 'Room is in read only mode'],401);
            }
        }
        $json = $request->input('data');
        $room->data = $json;
        $room->save();
        $data_generated = $room;
        return response()->json(array('success' => true, 'data_generated'=>$data_generated));
    }
    public function getBoard($secret)
    {
        $room = Room::findOrFail($secret);
        $json = $room->data;
        return response()->json(array('success' => true, 'json'=>$json));
    }
}
