<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        $email = $request->email;
        $password = $request->password;
        //If the user checked the remember me checkbox then make sure to remember him
        if(isset($request->rememberMe)) {
            if (Auth::attempt(['email' => $email, 'password' => $password], true)) {
                $user = User::findOrFail($email);
                //Remember the user
                Auth::login($user, true);
            } else {
                //Otherwise redirect back to login page with errors.
                return redirect()->back()->withInput()->withErrors('Invalid username or password');
            }
            //If the user didn't check the remember me checkbox then don't remember him.
        } else {
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                $user = User::findOrFail($email);
                Auth::login($user);
            } else {
                //Otherwise redirect back to login page with errors.
                return redirect()->back()->withInput()->withErrors('Invalid username or password');
            }
        }
    }
    /*
     * Logs the user out
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
