<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function getUser(Request $request)
    {
//        $user = auth()->user();
//        if (null === $user) {
//            throw new AccessDeniedHttpException('Access Denied');
//        }
        $user_id = DB::table('sessions')->where('id',$request->sessionId)->value('user_id');
        if($user_id == null)
        {
            return response()->json(["Couldn't authenticate user."],401);
        }
        return response()->json(['user' => User::find($user_id)],200);
//        return session()->getId();
//        if(\auth()->user())
//        {
//            return response()->json(['user' => Auth::user()],200);
//        } else {
//            return response()->json(['error' => session()->getId()], 401);
//        }
    }
}
