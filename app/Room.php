<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Room extends Model
{
    protected $primaryKey = 'secret';
	protected $fillable = ['secret','admin','readonly'];
	public $timestamps = false;
}
