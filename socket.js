let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let fetch = require('node-fetch');
require('dotenv').config({path:'.env'});
let request = require('request');
http.listen(3000);

let users = [], users_connected = [];
function findUsersConnected(room, wantsSocketId = false,namespace) {
    let names = [];
    let ns = io.of(namespace || "/");
    for (socketID in io.nsps['/'].adapter.rooms[room].sockets) {
      if(!wantsSocketId)   nickname = io.nsps['/'].connected[socketID].username;
      else             nickname = {username:`${io.nsps['/'].connected[socketID].username}`,socketId:`${io.nsps['/'].connected[socketID].id}`};
        names.push(nickname);
    }
    return names;
}
let isAuth;
let username;
let isAdmin;
const getAdmin = async (roomId) => {
    let response = await fetch(process.env.APP_URL + '/room/' + roomId + '/admin');
    let json = await response.json();
    return json.admin;
  };
io.use(function (socket, next) {
    request.post({
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url:     process.env.APP_URL + '/user/auth',
        body:    "sessionId="+socket.handshake.query.token,
        json:true
    }, function(error, response, body) {
        if(response.statusCode != 200)
        {
            isAuth = false;
            next();
            return;
        }
        isAuth = true;
        username = body.user.username;
        next();
    });
});
io.on('connection', function(socket) {
    // register the new user
    // let uid = null;
    let user = {username:""};
    socket.on('register', function (user_uid,room_id) {
        getAdmin(room_id).then((admin) => {
            socket.admin = admin;
        });
        if(!isAuth) {
            socket.username = user_uid;
        } else {
            user_uid = username;
            socket.username = user_uid;
        }
           socket.room_id = room_id;
        // if ( users_connected.indexOf(user_uid) < 0 ) {
            users_connected.push(user_uid);
        // }
        if ( users.indexOf(user_uid) < 0 ) {
            users.push(user_uid);
            socket.join(room_id);
            // notify other clients that a new user has joined
            socket.broadcast.to(room_id).emit('user:join', {
                name: user_uid,
                // users:users_connected
                users: users_connected
            });
            io.sockets.in(room_id).emit('get:users', {users:findUsersConnected(room_id),admin:getAdmin(room_id)});
            // if(users_connected.length == 1) {
            //     // socket.isAdmin = true;
            //     // user.isAdmin = true;
            // }
        }
            user.username = user_uid;
    });
    socket.on('shape:added',function(object)
    {
        io.sockets.in(socket.room_id).emit('shape:added', {object:object,user:user});
    });
    socket.on('draw:again',function(json)
    {
        io.sockets.in(socket.room_id).emit('draw:again', {json:json});
    });
    socket.on('draw:public',function(json)
    {
        io.sockets.in(socket.room_id).emit('draw:public', {json:json});
    });
    socket.on('read:only',function(state)
    {
        io.sockets.in(socket.room_id).emit('read:only',{state:state});
    });
    socket.on('drawing',function (username) {
        // io.sockets.in(socket.room_id).emit('drawing',{user:socket.username});
        socket.broadcast.to(socket.room_id).emit('drawing', {
            user:socket.username
        });
    });
    socket.on('chat message',function (message,user) {
        io.sockets.in(socket.room_id).emit('chat message', {
            message:message,
            user:socket.username,
        })
    });
    
    socket.on('stopped:drawing',function (username) {
        // io.sockets.in(socket.room_id).emit('drawing',{user:socket.username});
        socket.broadcast.to(socket.room_id).emit('stopped:drawing', {
            user:socket.username
        });
    });
    socket.on('kick:user',function (room_id,userToKick) {
        let usersInRoom = findUsersConnected(room_id);
        //Get every user in room along with the socket id so we can kick him
        //TODO:refactor this sometime so we don't have to init an array twice (only difference is that one has a socket id while the other doesnt).
        let usersInRoomWithSocketId = findUsersConnected(room_id,true);
        const admin =  getAdmin(room_id)
        //If the user wanted to kick himself for some reason
        if(admin == userToKick){
            console.log('you cant kick yourself');
        }
        //If the userToKick wasn't able to be found in room
        if(!usersInRoom.includes(userToKick)) {
            console.log('user wasnt found ');
        }
            //If user who wants to kick isn't admin
        if(!socket.admin || socket.username != socket.admin) {
            console.log('you dont have the permission to kick');
            return;
        }
        //If everything is valid loop through the usersInRoomWithSocketId array and get the user we need to kick socket's id and then kick him.
        for(let i = 0; i<usersInRoomWithSocketId.length; i++)
        {
            console.log('reached here');
           if(usersInRoomWithSocketId[i].username == userToKick)
           {
               let socketIdToKick = usersInRoomWithSocketId[i].socketId;
               io.sockets.to(socketIdToKick).emit('redirectAfterKick',socketIdToKick);
               io.sockets.connected[socketIdToKick].disconnect();
               console.log('kicked' + socketIdToKick);
           }
        }
    });
    // clean up when a user leaves, and broadcast it to other users
    socket.on('disconnect', function () {
        users_connected.splice( users_connected.indexOf(user.username), 1);
            if ( users_connected.indexOf(user.username) < 0 ) {
                socket.broadcast.to(socket.room_id).emit('user:left', {
                    user:user
                });
                let index = users.indexOf(user.username);
                users.splice(index, 1);
            }
    });

});

http.listen(3000, function(){
    console.log('listening on *:3000');
});