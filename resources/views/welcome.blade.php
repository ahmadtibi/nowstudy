<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/main.css')}}"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="{{ asset('js/home.js')  }}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.14/semantic.min.js"></script>
</head>
    <body class="pushable">
    <!-- Sidebar Menu -->
    <div class="ui vertical inverted sidebar menu left">
        <a class="active item">Home</a>
        <a class="item">Work</a>
        <a class="item">Company</a>
        <a class="item">Careers</a>
        <a class="item">Login</a>
        <a class="item">Signup</a>
    </div>

    <!-- Page Contents -->
    <div class="pusher">
        <div class="ui inverted vertical masthead center aligned segment introduction">
            <div class="ui container">
                <div class="ui large secondary inverted pointing menu introduction-menu">
                    <a class="toc item">
                        <i class="sidebar icon"></i>
                    </a>
                    <a class="active item">Home</a>
                    <div class="right item">
                        <a class="ui labeled icon button" href="/register"><i class="user plus icon"></i> Sign up</a>
                        <a class="ui labeled icon button" href="/login"><i class="user circle icon"></i> Login</a>
                        {{Auth::user()}}
                    </div>
                </div>
            </div>

            <div class="ui text container">
                <h1 class="ui inverted header introduction-header">
                    nowstudy
                </h1>
                <h2>Online whiteboards with collaboration. No sign up required and completely free &#x1F60B;</h2>
                    <div class="ui huge primary button" id="get-started" data-tooltip="No sign up required" data-inverted="">Get Started <i class="right arrow icon"></i></div>
            </div>

        </div>

        <div class="ui vertical stripe segment">
            <div class="ui aligned stackable three column grid container">
                <div class="column">
                    <h3 class="ui header">
                        <i class="magic icon"></i> Easy to use
                    </h3>
                    <p>Simply <a href="javascript:void(0)" class="create-room">create a room (for free)</a> and we will set up everything for you! it takes seconds</p>
                </div>
                <div class="column">
                    <h3 class="ui header">
                        <i class="laptop icon"></i> No sign up required
                    </h3>
                    <p>You don't have to create an account in order to create a room however creating an account might be useful as it gives you extra features and more control over the room</p>
                </div>
                <div class="column">
                    <h3 class="ui header">
                        <i class="send outline icon"></i> Lightweight and packed with features
                    </h3>
                    <p>Nowstudy has alot of built in features to make everything easier and more fun.</p>
                </div>
            </div>
        </div>
        <div class="ui vertical stripe segment">
            <div class="ui text container">
                <h3 class="ui center aligned header">
                    <i class="magic icon"></i> Magical Features
                </h3>
                <h3 class="ui horizontal header divider">
                    Interactive objects
                </h3>
                <h4 class="ui header">
                    You can <i>literally</i> interact with anything on the board. case in point you can move any shape, scale it, rotate it and even delete it.
                </h4>
                <img class="ui massive zoom-in image" src="../images/example-4.png">
                <h3 class="ui horizontal header divider">
                    Equation writer
                </h3>
                <h4 class="ui header">
                    The equation writer tool is powered by <a href="https://www.latex-project.org/">latex </a> one of the world's most powerful document preparation system. You can write any complicated equation with just one click that's just how simple we made it
                </h4>
                <img class="ui massive zoom-in image" src="../images/example-1.png">
                <h3 class="ui horizontal header divider">
                     Record screen
                </h3>
                <h4 class="ui header">
                    Record your screen at high quality.
                </h4>
                <img class="ui massive zoom-in image" src="../images/example-2.png">
                <h3 class="ui horizontal header divider">
                    Upload images
                </h3>
                <h4 class="ui header">
                    Solving a math question and having hard time remembering the question details and don't want to switch tabs or look at the book for the question details? We got that covered you can now upload images to the whiteboard at high quality
                </h4>
                    <img class="ui massive zoom-in image" src="../images/example-3.png">
            </div>
        </div>

        <div class="ui vertical stripe segment">
            <h3 class="ui center aligned header">
                <i class="image icon"></i> Cool user submitted stuff
            </h3>
            <div class="ui massive images">
                <img class="ui image">
            </div>
        </div>
    </div>
    {{--Create room modal--}}
    <div class="ui modal small create-room_modal">
        <i class="close icon"></i>
        <div class="header">
            Create a room
        </div>
        <div class="content">
            @if(!auth()->user())
            <div class="ui tiny warning message">
                <div class="header">
                    You won't have full control over your room since you aren't registered (no admin in room)
                </div>
                <a href="/register">Click here</a> to sign up for better control
            </div>
            @endif
            <div class="ui form">
                <div class="field">
                    @if(!auth()->user())
                    <label>Username (required)</label>
                    @endif
                    <input type="text" id="username" placeholder="what you would like to be called in the room" required>
                </div>
            </div>
        </div>
        <div class="actions">
            <div class="ui labeled icon positive button" id="create-room_submit"><i class="paper plane icon"></i> Create room</div>
        </div>
    </div>
    </body>
    <script src="{{asset('js/home.js')}}"></script>
</html>
