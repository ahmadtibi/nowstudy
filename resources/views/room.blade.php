<html>
<head>
    <script src="{{request()->getSchemeAndHttpHost()}}:3000/socket.io/socket.io.js"></script>

</head>
@section('content')
    {{auth()->user()    }}
    @extends('navbar')
    <script>
        window.sessionId = {!! json_encode(session()->getId()) !!};
        {{--window.sessionId = {{!! session()->getId() !!}}--}}
    </script>
    <script src="{{ asset('js/room.js')  }}"></script>
    <script src="{{ asset('js/room-socket.js')  }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <body class="room-cover">
    <div class="container">
        <div>
            <div class="ui vertical labeled icon buttons" id="hover-options">
                <button class="ui button" id="pasteObject">
                    <i class="paste icon"></i>
                    Paste
                </button>
                <button class="ui button" id="copyObject">
                    <i class="copy icon"></i>
                    Copy
                </button>
                <button class="ui button" id="deleteObject">
                    <i class="cut icon"></i>
                    Cut
                </button>
                {{--<button class="ui button">--}}
                    {{--<i class="shuffle icon"></i>--}}
                {{--</button>--}}
            </div>
            <canvas class="board" id="c"></canvas>
        </div>
        <div class="thickness-slider" data-tooltip="Change pencil's thickness" data-inverted="" data-position="left center">
            <input type="range" min="1" max="10" step="1" value="1" id="thickness">
            <p class="thickness-value">1</p>
            <p class="drawing" style="color:green;"></p>
            <ul class="name-list"></ul>
        </div>
        <div class="tools">
            <ul class="tools-list">
              <li class="tools-item">
                <div class="tools-select cursor-select" data-tooltip="Cursor (Shift + A)" data-inverted="" data-position="right center">
                  <i class="mouse pointer icon tool-icon" id="cursor"></i>
                </div>
              </li>
                <li class="tools-item">
                    <div class="tools-select pencil-select" data-tooltip="Pencil (Shift + E)" data-inverted="" data-position="right center">
                       <i class="write icon tool-icon" id="pencil"></i>
                    </div>
                </li>
                <li class="tools-item">
                    <div class="tools-select line-select" data-tooltip="Line (Shift + S)" data-inverted="" data-position="right center">
                        <i class="long arrow right icon tool-icon" id="line"></i>
                    </div>
                </li>
                <li class="tools-item">
                    <div class="tools-select square-select" data-tooltip="Square (Shift + R)" data-inverted="" data-position="right center">
                      <i class="square outline icon tool-icon" id="square"></i>
                    </div>
                </li>
                <li class="tools-item">
                  <div class="tools-select" data-tooltip="Undo (Control + Z)" data-inverted="" data-position="right center">
                      <i class="undo icon tool-icon" id="undo"></i>
                  </div>
                </li>
                <li class="tools-item">
                  <div class="tools-select circle-select" data-tooltip="Circle (Shift + C)" data-inverted="" data-position="right center">
                      <i class="circle thin icon tool-icon" id="circle"></i>
                  </div>
                </li>
                {{--<li class="tools-item">--}}
                    {{--<div class="tools-select eraser-select" data-tooltip="Eraser (Shift + E)" data-inverted="" data-position="right center">--}}
                        {{--<i class="eraser icon tool-icon" id="eraser"></i>--}}
                    {{--</div>--}}
                {{--</li>--}}
                <li class="tools-item">
                    <div class="tools-select color-picker" data-tooltip="Color picker" data-inverted="" data-position="right center">
                        <i class="eyedropper icon tool-icon" id="color-picker"></i>
                    </div>
                    <div class="popover">
                        <div class="popover-content" style="display:none;">
                            <ul class="popover-list">
                                <li class="popover-item popover-item-selected black">
                                    <a class="ui black circular label" id="black"> </a>
                                </li >
                                <li class="popover-item red">
                                    <a class="ui red circular label" id="red"> </a>
                                </li>
                                <li class="popover-item teal">
                                    <a class="ui teal circular label" id="teal"> </a>
                                </li>
                                <li class="popover-item grey">
                                    <a class="ui grey circular label" id="grey"> </a>
                                </li>
                                <li class="popover-item brown">
                                    <a class="ui brown circular label" id="brown"> </a>
                                </li>
                                <li class="popover-item green">
                                    <a class="ui green circular label" id="green"> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <li class="tools-item">
                    <div class="tools-select color-picker" data-tooltip="Opens up the calculator" data-inverted="" data-position="right center">
                        <i class="calculator icon tool-icon"></i>
                    </div>
                    <form name="sci-calc" class="sci-calc" style="display:none;">
                        <table id="calculator" cellspacing="0" cellpadding="1">
                            <tr>
                                <td colspan="5"><input id="display" name="display"></td>
                            </tr>
                            <tr>
                                <td><input type="button" class="btnTop" name="btnTop" id="clearDisplay" value="C"></td>
                                <td><input type="button" class="btnTop" name="btnTop" id="deleteChar" value="<--"></td>
                                <td><input type="button" class="btnTop" name="btnTop" value="=" id="compute"></td>
                                <td><input type="button" class="btnOpps" name="btnOpps" value="&#960;"></td>
                                <td><input type="button" class="btnMath" name="btnMath" value="%"></td>
                            </tr>
                            <tr>
                                <td><input type="button" class="btnNum" name="btnNum" value="7"></td>
                                <td><input type="button" class="btnNum" name="btnNum" value="8"></td>
                                <td><input type="button" class="btnNum" name="btnNum" value="9"></td>
                                <td><input type="button" class="round" name="btnOpps" value="round"></td>
                                <td><input type="button" class="btnMath" name="btnMath" value="/"></td>
                            <tr>
                                <td><input type="button" class="btnNum" name="btnNum" value="4"></td>
                                <td><input type="button" class="btnNum" name="btnNum" value="5"></td>
                                <td><input type="button" class="btnNum" name="btnNum" value="6"></td>
                                <td><input type="button" class="log" name="btnOpps" value="log"></td>
                                <td><input type="button" class="btnMath" name="btnMath" value="*"></td>
                            </tr>
                            <tr>
                                <td><input type="button" class="btnNum" name="btnNum" value="1"></td>
                                <td><input type="button" class="btnNum" name="btnNum" value="2"></td>
                                <td><input type="button" class="btnNum" name="btnNum" value="3"></td>
                                <td><input type="button" class="sqrt" name="btnOpps" value="&radic;"></td>
                                <td><input type="button" class="btnMath" name="btnMath" value="-"></td>
                            </tr>
                            <tr>
                                <td><input type="button" class="absolute_value" name="btnMath" value="&#177"></td>
                                <td><input type="button" class="btnNum" name="btnNum" value="0"></td>
                                <td><input type="button" class="btnMath" name="btnMath" value="&#46;"></td>
                                <td><input type="button" class="exponentiation" name="btnOpps" value="x&#50;"></td>
                                <td><input type="button" class="btnMath" name="btnMath" value="+"></td>
                            </tr>
                            <tr>
                                <td><input type="button" class="btnMath" name="btnMath" value="("></td>
                                <td><input type="button" class="btnMath" name="btnMath" value=")"></td>
                                <td><input type="button" class="trigonometric_function" value="sin"></td>
                                <td><input type="button" class="trigonometric_function" value="cos"></td>
                                <td><input type="button" class="trigonometric_function" value="tan"></td>
                            </tr>
                        </table>
                    </form>
                </li>
                </li>
                <li class="tools-item">
                    <div class="tools-select" data-tooltip="Upload an image to the whiteboard" data-inverted="" data-position="right center">
                        <i class="file image outline icon tool-icon" id="upload">
                            <input type="file" id="file">
                        </i>
                    </div>
                <li class="tools-item">
                    <div class="tools-select" data-tooltip="Write an equation on the board" data-inverted="" data-position="right center">
                        <i class="edit outline icon tool-icon" id="equation">
                        </i>
                    </div>
                </li>
                <li class="tools-item">
                    <div class="tools-select" data-tooltip="Draws a graph of a function" data-inverted="" data-position="right center">
                        <i class="chart line icon tool-icon" id="equation-drawer">
                        </i>
                    </div>
                </li>
                </li>
            </ul>
        </div>
        <div class="dashboard">
            <p id="roomSecret" hidden>{{Request::segment(2)}}</p>
<ul class="dashboard-items">
    <li class="dashboard-item">
        <a href="javascript:void(0)">
            <button class="ui clipboard green button"  data-tooltip="Copies room's secret key to clipboard" data-inverted="" id="copy">
                <i class="cut icon"></i>
                Share board
            </button>
        </a>
    </li>
    <li class="dashboard-item">
        <a href="javascript:void(0)">
            <button class="ui violet button" data-tooltip="Takes screenshot of the whiteboard" data-inverted="" id="screenshot">
                <i class="photo icon"></i>
                Snapshot
            </button>
        </a>
    </li>
    <li class="dashboard-item">
        <button class="ui black button" data-tooltip="Clears the board (Shift + Delete)" data-inverted="" id="clear">
            <i class="erase icon"></i>
            Clear board
        </button>
    </li>
    <li class="dashboard-item">
        <button class="ui purple button" data-tooltip="This records the canvas and saves it in the webm format (if you're using chrome make sure that your mic isnt blocked)" data-inverted="" id="record">
            <i class="record icon"></i>
            Record screen
            <div class="floating ui teal label small">Beta</div>
        </button>
    </li>
    <li class="dashboard-item">
        <button class="ui red button disabled" data-tooltip="" data-inverted="" id="stop-recording">
            <i class="record icon"></i>
            Stop Recording
        </button>
    </li>
    <li class="dashboard-item">
        <button class="ui primary button" data-tooltip="View the users in the room" data-inverted="" id="manage">
            <i class="eye icon"></i>
                        Users in room
        </button>
    </li>
    <li class="dashboard-item" style="float:right; margin-right: 100px;">
        <button class="ui fluid violet medium button" data-tooltip="Chat" data-inverted="" id="open-chat">
            <i class="paper plane icon"></i>
            Chat
            <div class="floating ui red label chat-counter">0</div>
        </button>
        <div class="chat-box hidden">
            <div class="chat-box-head">
                <i class="user outline icon"></i>
                <i class="window minimize icon close-chat" style="float:right; cursor:pointer;"></i>
            </div>
            <div class="chat-box-messages">
                <ul class="messages-list">
                </ul>
            </div>
            <div class="chat-box-send">
                <textarea id="send-message" rows="10" cols="40" placeholder="Press enter to send message"></textarea>
            </div>
        </div>
    </li>
    {{--Check if user is the actual admin of the room--}}
    @if(auth()->user() != null && auth()->user()->username == $room->admin)
    <li class="dashboard-item">
        @if(!$room->readonly)
        <button class="ui negative toggle button" data-tooltip="Prevents other users from drawing anything" id="read-only">
            <i class="cogs icon"></i>
            <span class="desc">
            Teacher mode
            </span>
        </button>
        @else
        <button class="ui negative toggle button active" data-tooltip="Allows others users to draw anything" id="read-only">
            <i class="cogs icon"></i>
            <span class="desc">
            Free mode        
        </span>
        </button>
        @endif
    </li>
    @endif
</ul>
            {{--Write Equation Modal--}}
            <div class="ui modal" id="equation-modal">
                <div class="header">Write equation (Note:The equation writer uses latex)</div>
                <div class="content">
                    <div class="ui form">
                        <div class="field">
                            <label>Equation:</label>
                            <input type="text" id="MathInput" value="\sum fy = n - mg = ma">
                        </div>
                    </div>
                    <h2 class="ui header">Preview (This is what the equation will look like on the board)</h2>
                        <div id="MathOutput" class="output"> $\sum fy = n - mg = ma$</div>
            </div>
                <div class="actions">
                    <div class="ui labeled icon positive button" id="equation-write"><i class="paper plane icon"></i> Write equation</div>
                    <div class="ui negative button">Cancel</div>
                </div>
            </div>
            {{--Write equation modal end--}}

            {{--Draw function modal --}}
            <div class="ui modal" id="function-drawer-modal">
                <div class="header">Draw function</div>
                <div class="content">
                    <div class="ui form">
                        <div class="field">
                            <label>Function</label>
                            <input type="text" id="FunctionInput">
                        </div>
                    </div>
                    <h2 class="ui header">Preview</h2>
                    <div id="plot"></div>
                </div>
                <div class="actions">
                    <div class="ui labeled icon primary button" id="function-draw"><i class="paper plane icon"></i> Write equation</div>
                    <div class="ui negative button">Cancel</div>
                </div>
            </div>
            {{--Draw function modal end--}}

            {{--Manage/view users modal--}}
            <div class="ui modal inverted" id="manage-users">
                <i class="close icon"></i>
                <div class="header">
                    Manage users
                </div>
                <div class="content">
                    <table class="ui inverted table">
                        <div class="ui left icon focus input">
                            <input placeholder="Search users..." type="text" id="userToFind">
                            <i class="users icon"></i>
                        </div>
                        <button class="ui small kickUser red button" data-_extension-text-contrast="" style="margin-left: 30px;">
                            Kick
                        </button>
                        <div class="ui visible mini positive message">
                            <i class="close icon" onclick="$(this).parent().hide()"></i>
                        </div>
                        <div class="ui visible mini error message">
                            <i class="close icon" onclick="$(this).parent().hide()"></i>
                        </div>
                        <br>
                        <thead>
                        <tr>
                            <th><i class="tag icon"></i> Name</th>
                            <th><i class="clock icon"></i> Time</th>
                        </tr>
                        </thead>
                        <tbody class="usersInRoom">
                        {{--<tr>--}}
                            {{--<td>AboTibi</td>--}}
                            {{--<td class="positive">Draw and view</td>--}}
                            {{--<td>--}}
                                {{--<button class="ui mini red labeled icon button">--}}
                                    {{--<i class="ban icon"></i>--}}
                                    {{--Kick--}}
                                {{--</button>--}}
                                {{--<button class="ui mini black labeled icon button" data-tooltip="Allows the user to only view and prevents him from drawing">--}}
                                    {{--<i class="eye slash icon"></i>--}}
                                    {{--View only--}}
                                {{--</button>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>Flex</td>--}}
                            {{--<td class="positive">Draw and view</td>--}}

                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>SariTibi</td>--}}
                            {{--<td class="positive">Draw and view</td>--}}
                        {{--</tr>--}}
                        </tbody>
                    </table>
                </div>
                <div class="actions">
                </div>
        </div>
            {{--Manage/view users modal end--}}
    </div>
    </div>

    </body>
@stop
</html>
