<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
@extends('navbar')
@section('content')
<body style="background-color:#69EAE4 !important;" class="room-join">
<div class="container">
<div class="ui centered grid container" style="padding-top: 300px;">
    <div class="seven wide column centered row">
       <h1 class="ui inverted header">Join Room</h1>
        <form class="ui form" id="room" action="javascript:void(0)">
            <div class="field joinRoom">
                <input name="RoomSecret" placeholder="Room secret key" id="RoomSecret" type="text" autofocus="">
                <small class="helperRoom" style="color:#db5656;"></small>
            </div>
            {{--If user is logged in--}}
            @if(auth()->user() != null)
            <div class="field joinRoom">
                <input name="username" placeholder="Username" id="username" type="text" value="{{auth()->user()->username}}" disabled>
                <small class="helperUsername" style="color:#db5656;"></small>
            </div>
            {{--If user isn't logged in--}}
            @else
                <div class="field joinRoom">
                    <input name="username" placeholder="Username" id="username" type="text">
                    <small class="helperUsername" style="color:#db5656;"></small>
                </div>
            @endif
            <button type="button" class="ui teal button" id="JoinRoom">Join Room</button>
        </form>
    </div>
</div>
</div>
</body>
<script src="{{asset('js/join.js')}}"></script>
@stop
</html>