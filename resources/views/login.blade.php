<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/main.css')}}"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.14/semantic.min.js"></script>
</head>
<body class="login-body">
<div class="ui middle aligned center aligned grid">
    <div class="six wide column">
        <h2 class="ui header" style="color:white;">
            <div class="content">
                Login
            </div>
        </h2>
        <form class="ui large form" method="POST" action="/user/login">
            {!! csrf_field() !!}
            <div class="ui stacked segment">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input name="email" placeholder="E-mail address" type="email">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input name="password" placeholder="Password" type="password">
                    </div>
                </div>
                <div class="field" style="float:left; padding-left: 3px;">
                    <div class="ui checkbox">
                        <input name="rememberMe" type="checkbox">
                        <label>Remember me</label>
                    </div>
                </div>
                <button class="ui fluid large teal submit button" type="submit">Login</button>
            </div>
            @if ($errors->any())
                <div class="ui error message" style="display:block !important;">
                    <ul class="list">
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </form>
        <div class="ui message">
            Don't have an account? <a href="/register">Sign Up </a> now it takes a few seconds
        </div>
    </div>
</div>
</body>
</html>