<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	{{--<script src="http://localhost:3000/socket.io/socket.io.js"></script>--}}
	{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css" /> --}}
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css" />
	<script type="text/x-mathjax-config">
  MathJax.Hub.Config({
    tex2jax: {
      inlineMath: [ ['$','$'], ["\\(","\\)"] ],
      processEscapes: true
    },
    jax: ["input/TeX","output/SVG"],
    SVG:{blacker:0}
  });
</script>
    <link rel="stylesheet" href="{{asset('css/main.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/range.css')}}"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css" />
	<script src="https://cdn.WebRTC-Experiment.com/RecordRTC.js"></script>
	<script src="https://cdn.webrtc-experiment.com/RecordRTC/CanvasRecorder.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>
	<script src="https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.0.0/fabric.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/3.20.2/math.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.14/semantic.min.js"></script>
	<script type="text/javascript"
			src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js">
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.3/jquery.timeago.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/config/TeX-AMS_HTML.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
	<script src="https://unpkg.com/mathjs@4.0.0/dist/math.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
	<script src="{{asset('js/function-plot.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
	{{--<script src="https://wzrd.in/standalone/function-plot@1.14.0"></script>--}}
</head>
<body>
<script language="javascript">
</script>
@yield('content')
{{--<footer><center>&copy; CopyRight 2018 - All Rights Reserved</center></footer>--}}
</body>
</html>
